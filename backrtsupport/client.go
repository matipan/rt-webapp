package main

import (
	"github.com/dancannon/gorethink"
	"github.com/gorilla/websocket"
)

type Message struct {
	Name string      `json:"name"`
	Data interface{} `json:"data"`
}

type FindHandler func(string) (Handler, bool)

type Client struct {
	send         chan Message
	socket       *websocket.Conn
	findHandler  FindHandler
	session      *gorethink.Session
	stopChannels map[int]chan bool
}

func (client *Client) NewStopChannel(stopKey int) (stop chan bool) {
	client.StopForKey(stopKey)
	stop = make(chan bool)
	client.stopChannels[stopKey] = stop
	return
}

func (client *Client) StopForKey(key int) {
	if ch, found := client.stopChannels[key]; found {
		stop <- true
		delete(client.stopChannels, key)
	}
}

func (client *Client) Read() {
	var message Message
	for {
		if err := client.socket.ReadJSON(&message); err != nil {
			break
		}
		if handler, found := client.findHandler(message.Name); found {
			handler(client, message.Data)
		}
	}
}

func (client *Client) Write() {
	for msg := range client.send {
		if err := client.socket.WriteJSON(msg); err != nil {
			break
		}
	}
	client.socket.Close()
}

func (client *Client) Close() {
	for _, ch := range client.stopChannels {
		ch <- true
	}
	close(client.send)
}

func NewClient(socket *websocket.Conn, findHandler FindHandler, session *gorethink.Session) *Client {
	return &Client{
		send:         make(chan Message),
		socket:       socket,
		findHandler:  findHandler,
		session:      session,
		stopChannels: make(map[int]chan bool),
	}
}
